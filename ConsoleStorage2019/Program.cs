﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ConsoleStorage2019
{
    class Program
    {
        /// <summary>
        /// Коллекция записей склада
        /// </summary>
        public static List<structStorageNote> listNotes = new List<structStorageNote>();

        /// <summary>
        /// Точка входа в приложение
        /// </summary>
        static void Main(string[] args)
        {
            bool isMenu = true;
            while (isMenu)
            {
                switch (Menu())
                {
                    case 0:
                        isMenu = false;
                        break;
                    case 1:
                        ViewAll();
                        break;
                    case 2:
                        Sort();
                        break;
                    case 3:
                        Add();
                        break;
                    case 4:
                        Remove();
                        break;
                    case 5:
                        Save();
                        break;
                    case 6:
                        Load();
                        break;
                    default:
                        break;
                }    
            }
        }

        /// <summary>
        /// Реализация главноо меню
        /// </summary>
        /// <returns>Номер пункта меню</returns>
        static int Menu()
        {
            // Очистка окна консоли 
            Console.Clear();
            //Выводим меню, его пункты с соответствующими цифрами\символами
            Console.Write("\tГлавное меню\n");
            Console.Write("\n\t[1]. Показать все записи");
            Console.Write("\n\t[2]. Выполнить сортировку");
            Console.Write("\n\t[3]. Добавить запись");
            Console.Write("\n\t[4]. Удалить запись");
            Console.Write("\n\t[5]. Сохранить в файл (xml)");
            Console.Write("\n\t[6]. Загрузить из файла (xml)");
            Console.Write("\n\n\t[0]. Выйти");
            Console.Write("\n\n\tНажмите соответствующую кнопку ");
            // Чтение нажатой кнопки
            switch (Console.ReadKey(false).Key)
            {
                case ConsoleKey.D1:
                    // Нажата кнопка 1
                    return 1;
                case ConsoleKey.D2:
                    // Нажата кнопка 2
                    return 2;
                case ConsoleKey.D3:
                    // Нажата кнопка 3
                    return 3;
                case ConsoleKey.D4:
                    // Нажата кнопка 4
                    return 4;
                case ConsoleKey.D5:
                    // Нажата кнопка 5
                    return 5;
                case ConsoleKey.D6:
                    // Нажата кнопка 6
                    return 6;
                case ConsoleKey.D0:
                    // Нажата кнопка 0
                    return 0;
                default:
                    // Нажата неизвестная кнопка 
                    return -1;
            }
        }

        /// <summary>
        /// Отобразить ошибку
        /// </summary>
        /// <param name="msg">Текст ошибки</param>
        static void ViewError(string msg)
        {
            Console.Write("\n\n\tОШИБКА!");
            Console.Write("\n\t" + msg + "\n");
            Console.Write("\n\tНажмите любую кнопку для продолжения...");
            Console.ReadKey(false);
        }

        /// <summary>
        /// Показать все записи в виде таблицы
        /// </summary>
        static void ViewAll()
        {
            // Очистка окна консоли 
            Console.Clear();
            Console.Write(" | {0,20} | ", "Наименование товара");
            Console.Write("{0,17} | ", "Инвентарный номер");
            Console.Write("{0,13} | ", "Дата поставки");
            Console.Write("{0,15} | ", "Закупочная цена");
            Console.Write("{0,14} | \n", "Отпускная цена");
            foreach (structStorageNote note in listNotes)
            {
                string name = note.getName();
                string num = note.getNum().ToString();
                string date = note.getDate();
                string price = note.getPrice().ToString();
                string price_finaly = note.getPriceFinaly().ToString();
                Console.WriteLine(" | {0,20} | {1,17} | {2,13} | {3,15} | {4,14} | ", name, num, date, price, price_finaly);
            }
            Console.Write("\n\tНажмите любую кнопку для продолжения...");
            Console.ReadKey(false);
        }

        /// <summary>
        /// Добавление новой записи в коллекцию Storage
        /// </summary>
        static void Add()
        {
            // Очистка окна консоли 
            Console.Clear();
            // Значение полей по умолчанию
            string name = "Товар_" + DateTime.Now.ToString("ddMMyyHHmmss");
            string num = DateTime.Now.ToString("ddMMyyHHmmss");
            string date = DateTime.Now.ToString("dd-MM-yyyy");
            string price = "0";
            string price_change = "0";
            // Ввод данных
            string str =  "";
            Console.Write("\n\tВведите наименование товара (<ENTER> по умолчанию "+ name + "): ");
            str = Console.ReadLine();
            if (str.Length > 0)name = str;
            Console.Write("\tВведите инвентарный номер (<ENTER> по умолчанию "+ num + "): ");
            str = Console.ReadLine();
            if (str.Length > 0) num = str;
            Console.Write("\tВведите дату поставки (<ENTER> по умолчанию " + date + "): ");
            str = Console.ReadLine();
            if (str.Length > 0) date = str;
            Console.Write("\tВведите закупочную цену (<ENTER> по умолчанию " + price + "): ");
            str = Console.ReadLine();
            if (str.Length > 0) price = str;
            Console.Write("\tВведите наценку (<ENTER> по умолчанию "+ price_change + "): ");
            str = Console.ReadLine();
            if (str.Length > 0) price_change = str;
            // Добавление записи в динамический массив
            try
            {
                listNotes.Add(new structStorageNote(name, Int64.Parse(num), DateTime.Parse(date), double.Parse(price), double.Parse(price_change)));
            }
            catch (Exception ex)
            {
                ViewError(ex.Message);
                return;
            }
            Console.Write("\n\tЗапись добавлена.");
            Console.Write("\n\tНажмите любую кнопку для продолжения...");
            Console.ReadKey(false);
        }

        /// <summary>
        /// Удаление записи из коллекции Storage по инвентарному номеру
        /// </summary>
        static void Remove()
        {
            // Очистка окна консоли 
            Console.Clear();
            // Ввод инвентарного номера
            Console.Write("\tВведите инвентарный номер: ");
            string num = Console.ReadLine();
            // Поиск индекса в динамическом массиве
            int index = listNotes.FindIndex(x => x.getNum().ToString() == num);
            if (index > 0)
            {
                Console.Write("\n\tЗапись " + listNotes[index].getName() + " удалена.");
                listNotes.RemoveAt(index);
            }
            else
            {
                Console.Write("\n\tЗапись не найдена.");
            }
            Console.Write("\n\tНажмите любую кнопку для продолжения...");
            Console.ReadKey(false);
        }

        /// <summary>
        /// Загрузка данных из файла
        /// </summary>
        static void Load()
        {
            // Очистка окна консоли 
            Console.Clear();
            // Путь до файла по умолчанию
            string path = Environment.CurrentDirectory + "\\storage.xml";

            // Ввод нового пути до файла
            Console.Write("\n\tЗагрузка данных.");
            Console.Write("\n\n\tВведите полный путь до файла (<ENTER> по умолчанию " + path + "): ");
            string str = Console.ReadLine();
            if (str.Length > 0)
            {
                path = str;
            }

            // Проверка наличия файла
            if (!File.Exists(path))
            {
                ViewError("Файл: " + path + " не найден.");
                return;
            }

            try
            {
                // Открыть файл для чтения данных
                FileStream stream = new FileStream(path, FileMode.Open);

                // Создание XML документа и загрузка в него данные
                XmlDocument doc = new XmlDocument();
                doc.Load(stream);
                stream.Close();

                // Добавление данных в динамическую коллекцию
                listNotes.Clear();

                foreach (XmlNode node in doc["storage"].ChildNodes)
                {
                    string name, num, date, price, price_change;
                    name = node.Attributes["name"].InnerText;
                    num = node.Attributes["num"].InnerText;
                    date = node.Attributes["date"].InnerText;
                    price = node.Attributes["price"].InnerText;
                    price_change = node.Attributes["price_change"].InnerText;
                    listNotes.Add(new structStorageNote(name, Int64.Parse(num), DateTime.Parse(date), double.Parse(price), double.Parse(price_change)));
                }
            }
            catch (Exception ex)
            {
                ViewError(ex.Message);
                return;
            }
        }

        /// <summary>
        /// Сохранение данных в файл (xml)
        /// </summary>
        static void Save()
        {
            // Очистка окна консоли 
            Console.Clear();

            // Путь до файла по умолчанию
            string path = Environment.CurrentDirectory + "\\storage.xml";

            // Ввод нового пути до файла
            Console.Write("\n\tСохранение данных.");
            Console.Write("\n\n\tВведите полный путь до файла (<ENTER> по умолчанию "+ path + "): ");
            string str = Console.ReadLine();
            if (str.Length > 0)
            {
                path = str;
            }

            try
            {
                // Создание нового файла (или выполнить перезапись старого)
                FileStream stream = new FileStream(path, FileMode.Create);

                // Создание XML документа
                XmlDocument doc = new XmlDocument();
                doc.LoadXml("<storage></storage>");

                // Создание структуры нового XML файла
                foreach (structStorageNote note in listNotes)
                {
                    XmlNode node = doc.CreateElement("row");
                    XmlAttribute attributeName = doc.CreateAttribute("name");
                    attributeName.Value = note.getName();
                    XmlAttribute attributeNum = doc.CreateAttribute("num");
                    attributeNum.Value = note.getNum().ToString();
                    XmlAttribute attributeDate = doc.CreateAttribute("date");
                    attributeDate.Value = note.getDate();
                    XmlAttribute attributePrice = doc.CreateAttribute("price");
                    attributePrice.Value = note.getPrice().ToString();
                    XmlAttribute attributePriceChange = doc.CreateAttribute("price_change");
                    attributePriceChange.Value = note.getPriceChange().ToString();
                    node.Attributes.Append(attributeName);
                    node.Attributes.Append(attributeNum);
                    node.Attributes.Append(attributeDate);
                    node.Attributes.Append(attributePrice);
                    node.Attributes.Append(attributePriceChange);
                    doc.DocumentElement.AppendChild(node);
                }

                // Сохранение файла
                doc.Save(stream);
                stream.Close();
            }
            catch (Exception ex)
            {
                ViewError(ex.Message);
                return;
            }
        }

        /// <summary>
        /// Сортировка
        /// </summary>
        static void Sort()
        {

            // Очистка окна консоли 
            Console.Clear();

            Console.Write("\n\tСортировка по закупочной цене: ");
            Console.Write("\n\t[1] - По возрастанию");
            Console.Write("\n\t[2] - По убыванию");
            Console.Write("\n\n\t[0] - Назад");

            // Чтение нажатой кнопки
            switch (Console.ReadKey(false).Key)
            {
                case ConsoleKey.D1:
                    // Нажата кнопка 1
                    listNotes = SortMinToMax();
                    break;
                case ConsoleKey.D2:
                    // Нажата кнопка 2
                    listNotes = SortMaxToMin();
                    break;
                case ConsoleKey.D0:
                    // Нажата кнопка 0
                    return;
                default:
                    break;
            }


          
        }

        /// <summary>
        /// Сортировка по возрастанию
        /// </summary>
        /// <returns>Отсортированная коллекция</returns>
        static List<structStorageNote> SortMinToMax()
        {
            // Проверка количества элементов в коллекции
            if (listNotes.Count == 0)
            {
                return listNotes;
            }

            // Инициализация новой коллекции для сортировки
            List<structStorageNote> listNotesSort = new List<structStorageNote>();

            // Число элементов в текущей коллекции
            int count = listNotes.Count;

            // Выполнение сортировки методом выбора
            while (listNotesSort.Count < count)
            {
                double min_price = listNotes[0].getPrice();
                int min_index = 0;

                for (int i = 0; i < listNotes.Count; i++)
                {
                    if (listNotes[i].getPrice() < min_price)
                    {
                        min_price = listNotes[i].getPrice();
                        min_index = i;
                    }
                }
                listNotesSort.Add(listNotes[min_index]);
                listNotes.RemoveAt(min_index);
            }

            // Возвращение новой коллекции
            return listNotesSort;
        }

        /// <summary>
        /// Сортировка по убыванию
        /// </summary>
        /// <returns>Отсортированная коллекция</returns>
        static List<structStorageNote> SortMaxToMin()
        {
            // Проверка количества элементов в коллекции
            if (listNotes.Count == 0)
            {
                return listNotes;
            }

            // Инициализация новой коллекции для сортировки
            List<structStorageNote> listNotesSort = new List<structStorageNote>();

            // Число элементов в текущей коллекции
            int count = listNotes.Count;

            // Выполнение сортировки методом выбора
            while (listNotesSort.Count < count)
            {
                double max_price = listNotes[0].getPrice();
                int max_index = 0;

                for (int i = 0; i < listNotes.Count; i++)
                {
                    if (listNotes[i].getPrice() > max_price)
                    {
                        max_price = listNotes[i].getPrice();
                        max_index = i;
                    }
                }
                listNotesSort.Add(listNotes[max_index]);
                listNotes.RemoveAt(max_index);
            }

            // Возвращение новой коллекции
            return listNotesSort;
        }
    }

    /// <summary>
    /// Структура записи для склада
    /// </summary>
    struct structStorageNote
    {
        /// <summary>
        /// Наименование товара
        /// </summary>
        private string name;
        /// <summary>
        /// Инвентарный номер
        /// </summary>
        private long num;
        /// <summary>
        /// Дата поставки
        /// </summary>
        private DateTime date;
        /// <summary>
        /// Закупочная цена
        /// </summary>
        private double price;
        /// <summary>
        /// Наценка
        /// </summary>
        private double price_charge;
        /// <summary>
        /// Отпускная цена
        /// </summary>
        private double price_finaly;
        public structStorageNote(string name, long num, DateTime date, double price, double price_charge)
        {
            this.name = name;
            this.num = num;
            this.date = date;
            this.price = price;
            this.price_charge = price_charge;
            this.price_finaly = this.price + this.price_charge;
        }
        /// <summary>
        /// Получить наименование товара
        /// </summary>
        /// <returns>Наименование товара</returns>
        public string getName()
        {
            return this.name;
        }
        /// <summary>
        /// Получить инвентарный номер
        /// </summary>
        /// <returns>Инвентарный номер</returns>
        public long getNum()
        {
            return this.num;
        }
        /// <summary>
        /// Получить дату поставки
        /// </summary>
        /// <returns>Дата поставки</returns>
        public string getDate()
        {
            return this.date.ToString("dd-MM-yyyy");
        }
        /// <summary>
        /// Получить закупочную цену
        /// </summary>
        /// <returns>Закупочная цена</returns>
        public double getPrice()
        {
            return this.price;
        }
        /// <summary>
        /// Получить отпускную цену
        /// </summary>
        /// <returns>Отпускная цена</returns>
        public double getPriceFinaly()
        {
            return this.price_finaly;
        }
        /// <summary>
        /// Получить наценку
        /// </summary>
        /// <returns>Наценка</returns>
        public double getPriceChange()
        {
            return price_charge;
        }


    }
}
